use csv::Reader;
use serde::Deserialize;
use std::error::Error;

#[derive(Debug, Deserialize)]
struct WineQualityRecord {
    fixedacidity: f64,
    volatileacidity: f64,
    citricacid: f64,
    residualsugar: f64,
    chlorides: f64,
    freesulfurdioxide: f64,
    totalsulfurdioxide: f64,
    density: f64,
    ph: f64,
    sulphates: f64,
    alcohol: f64,
    quality: f64, 
}

fn main() -> Result<(), Box<dyn Error>> {
    let file_path = "winequality-red.csv";
    let mut rdr = Reader::from_path(file_path)?;
    
    let mut sum_fixed_acidity = 0.0;
    let mut sum_volatile_acidity = 0.0;
    let mut sum_citric_acid = 0.0;
    let mut sum_residual_sugar = 0.0;
    let mut sum_chlorides = 0.0;
    let mut sum_free_sulfur_dioxide = 0.0;
    let mut sum_total_sulfur_dioxide = 0.0;
    let mut sum_density = 0.0;
    let mut sum_ph = 0.0;
    let mut sum_sulphates = 0.0;
    let mut sum_alcohol = 0.0;
    let mut sum_quality = 0.0;
    let mut count = 0;

    for result in rdr.deserialize() {
        let record: WineQualityRecord = result?;
        sum_fixed_acidity += record.fixedacidity;
        sum_volatile_acidity += record.volatileacidity;
        sum_citric_acid += record.citricacid;
        sum_residual_sugar += record.residualsugar;
        sum_chlorides += record.chlorides;
        sum_free_sulfur_dioxide += record.freesulfurdioxide;
        sum_total_sulfur_dioxide += record.totalsulfurdioxide;
        sum_density += record.density;
        sum_ph += record.ph;
        sum_sulphates += record.sulphates;
        sum_alcohol += record.alcohol;
        sum_quality += record.quality;
        count += 1;
    }

    if count > 0 {
        println!("Average fixed acidity: {}", sum_fixed_acidity / count as f64);
        println!("Average volatile acidity: {}", sum_volatile_acidity / count as f64);
        println!("Average citric acid: {}", sum_citric_acid / count as f64);
        println!("Average residual sugar: {}", sum_residual_sugar / count as f64);
        println!("Average chlorides: {}", sum_chlorides / count as f64);
        println!("Average free sulfur dioxide: {}", sum_free_sulfur_dioxide / count as f64);
        println!("Average total sulfur dioxide: {}", sum_total_sulfur_dioxide / count as f64);
        println!("Average density: {}", sum_density / count as f64);
        println!("Average pH: {}", sum_ph / count as f64);
        println!("Average sulphates: {}", sum_sulphates / count as f64);
        println!("Average alcohol: {}", sum_alcohol / count as f64);
        println!("Average quality: {}", sum_quality / count as f64);
    } else {
        println!("No records found in the CSV file.");
    }

    Ok(())
}
