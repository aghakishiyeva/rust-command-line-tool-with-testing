# Wine Quality Analyzer

This Rust command-line tool analyzes the quality of wines based on data provided in a CSV file.

## Features

- Reads wine quality data from a CSV file.
- Calculates and prints average values for various attributes of wine quality.
- Handles CSV parsing errors and missing data gracefully.

## Usage

1. Make sure you have Rust installed on your system.
2. Clone this repository to your local machine.
3. Navigate to the project directory.
4. Place your CSV file containing wine quality data in the project root directory with the name `winequality-red.csv`.
5. Run the following command to execute the program:

```bash
cargo run
```

## CSV File Format

The CSV file should contain the following columns:

- `fixedacidity`: Fixed acidity of the wine.
- `volatileacidity`: Volatile acidity of the wine.
- `citricacid`: Citric acid content in the wine.
- `residualsugar`: Residual sugar content in the wine.
- `chlorides`: Chlorides content in the wine.
- `freesulfurdioxide`: Free sulfur dioxide content in the wine.
- `totalsulfurdioxide`: Total sulfur dioxide content in the wine.
- `density`: Density of the wine.
- `ph`: pH value of the wine.
- `sulphates`: Sulfates content in the wine.
- `alcohol`: Alcohol content in the wine.
- `quality`: Quality rating of the wine.

## Command Line Output

![Wine Quality Analyzer](https://i.ibb.co/sKGfWgw/Screenshot-2024-04-02-at-9-33-57-PM.png)



